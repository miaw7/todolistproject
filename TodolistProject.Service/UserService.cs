﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Repository.Dapper.Repositories;

namespace TodolistProject.Service
{
    public class UserService
    {
        public void BanUser(string UserId)
        {
            UserRepository repos = new UserRepository();
            repos.BanUser(UserId);
        }

        public void UnbanUser(string UserId)
        {
            UserRepository repos = new UserRepository();
            repos.UnbanUser(UserId);
        }


        public void DeleteUserData(string Id)
        {
            UserRepository repos = new UserRepository();
            repos.DeleteUserData(Id);
        }
    }
}
