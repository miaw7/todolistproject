﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Domain.Enums;
using TodolistProject.Domain.Models;
using TodolistProject.Repository.Dapper.Repositories;
using TodolistProject.Service.Dtos;

namespace TodolistProject.Service
{
    public class UserTaskService
    {
        private BaseRepository<UserTask> _repository;    

        public UserTaskService()
        {
            _repository = new BaseRepository<UserTask>("UserTasks");
        }

        public void AddTask(UserTaskDto task)
        {
            UserTask dbTask = new UserTask();
            dbTask.TaskName = task.TaskName;
            dbTask.FinishDate = DateTime.Now;
            dbTask.TaskDescription = task.TaskDescription;
            dbTask.UserId = task.UserId;
            dbTask.Priority = task.Priority;
            _repository.Add(dbTask);
        }

        public List<UserTaskDto> GetAllTasks(string UserId)
        {
            UserRepository repos = new UserRepository();
            IEnumerable<UserTask> UserTasks = repos.GetAllTaskForLogginedUser(UserId);
            var UserTasksDtos = new List<UserTaskDto>();
            foreach (var item in UserTasks)
            {
                UserTaskDto dto = new UserTaskDto();
                dto.Id = item.Id;
                dto.TaskDescription = item.TaskDescription;
                dto.TaskName = item.TaskName;
                dto.Status = item.Status;
                dto.Priority = item.Priority;
                UserTasksDtos.Add(dto);
            }
            return UserTasksDtos;
        }

        public void RemoveTask(string id)
        {
            UserRepository repos = new UserRepository();
            var task = repos.GetById(id);
            task.Status = UserTaskStatus.Deleted.ToString();
            repos.Update(task);
            return;
        }
    }
}
