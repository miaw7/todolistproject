﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Service.Dtos;
using TodolistProject.Repository.Dapper.Repositories;
using TodolistProject.Domain.Models;

namespace TodolistProject.Service
{
    public class RecaptchaService
    {
        private RecaptchaRepository _RecaptchRepos;
        public RecaptchaService()
        {
            _RecaptchRepos = new RecaptchaRepository();
        }
        public RecaptchaLock GetLockUser(string UserIp)
        {
            return _RecaptchRepos.GetLockedUser(UserIp);
        }

        public RecaptchaLock SetLockUser(string UserIp)
        {
            var model = new RecaptchaLock();
            model.UserIpAdress = UserIp;
            model.FailedAmount = 1;
            model.LockTime = DateTime.UtcNow.AddMinutes(2);
            _RecaptchRepos.Add(model);
            return model;
        }

        public void RemoveLock(string UserIp)
        {
            var model = _RecaptchRepos.GetLockedUser(UserIp);
            _RecaptchRepos.Remove(model);
        }

        public RecaptchaLock IncrementLock(RecaptchaLock User)
        {
            if(User.FailedAmount<3)
            {
                User.FailedAmount++;
                _RecaptchRepos.Update(User);
            }
            return User;
        }
    }
}
