﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Config;
using Microsoft.AspNetCore.Http; //need to install nuGet microsoft.AspNet.Http
using Amazon.S3;
using Amazon;
using Amazon.S3.Transfer;

namespace TodolistProject.Service
{
    public class ImageService
    {
        public string UploadImage(IFormFileCollection files)
        {
            var extImg = Path.GetExtension(files[0].FileName);

            string fileNameImg = Guid.NewGuid().ToString();

            using (var client = new AmazonS3Client(AppConfig.AccesKeyS3, AppConfig.SecretKeyS3, RegionEndpoint.EUCentral1))
            {
                var fileTransferUtility = new TransferUtility(client);

                using (var newMemoryStream = new MemoryStream())
                {
                    var imageObj = files[0];

                    imageObj.CopyTo(newMemoryStream);
                    var uploadImageRequest = new TransferUtilityUploadRequest
                    {
                        InputStream = newMemoryStream,
                        Key = fileNameImg + extImg,
                        BucketName = "agregator-project"
                    };

                    fileTransferUtility.Upload(uploadImageRequest);
                }
            }

            return $"{AppConfig.HostNameS3}{AppConfig.BucketNameS3}/{fileNameImg}{extImg}";
        }
    }
}
