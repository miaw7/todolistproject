﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using TodolistProject.Core.Models.UserModels;
using TodolistProject.Service;
using Microsoft.AspNetCore.Http;
using TodolistProject.Core.Recaptcha;

namespace TodolistProject.Core.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        #region Prop
        private readonly SignInManager<CommonUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly RecaptchaService _recaptchaService;
        private readonly IHttpContextAccessor _request;

        public LoginModel(SignInManager<CommonUser> signInManager, ILogger<LoginModel> logger, IHttpContextAccessor request)
        {
            _signInManager = signInManager;
            _logger = logger;
            _recaptchaService = new RecaptchaService();
            _request = request;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }

            public bool UseCaptcha { get; set; }
        }

        #endregion

        public async Task OnGetAsync(string returnUrl = null)
        {
            //Recapcha check START
            Input = new InputModel();
            var RecaptchaUser = _recaptchaService.GetLockUser(_request.HttpContext.Connection.RemoteIpAddress.ToString());

            if (RecaptchaUser != null)
            {
                if (RecaptchaUser.FailedAmount == 3)
                {
                    if(RecaptchaUser.LockTime < DateTime.UtcNow)
                    {
                        _recaptchaService.RemoveLock(_request.HttpContext.Connection.RemoteIpAddress.ToString());
                        Input.UseCaptcha = false;
                    }
                    else
                    Input.UseCaptcha = true;
                }
            }
            //Recapcha check END
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            var RecaptchaUser = _recaptchaService.GetLockUser(_request.HttpContext.Connection.RemoteIpAddress.ToString());
            if (RecaptchaUser != null && RecaptchaUser.FailedAmount == 3)
            {
                var valid = new RecaptchaCheckIsValid();
                if (!await valid.Validate(Request.Form["g-recaptcha-response"]))
                {
                    ModelState.AddModelError(string.Empty, "You failed the CAPTCHA");
                    Input.UseCaptcha = true;
                }
            }
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    if(RecaptchaUser != null)
                    _recaptchaService.RemoveLock(_request.HttpContext.Connection.RemoteIpAddress.ToString());
                    return LocalRedirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToPage("./Lockout");
                }
                else
                {

                    if (RecaptchaUser != null)
                    {
                        _recaptchaService.IncrementLock(RecaptchaUser);
                        if (RecaptchaUser.FailedAmount < 3)
                        {
                            Input.UseCaptcha = false;
                        }
                        else
                        {
                            Input.UseCaptcha = true;
                        }
                    }
                    else
                    {
                        _recaptchaService.SetLockUser(_request.HttpContext.Connection.RemoteIpAddress.ToString());
                        Input.UseCaptcha = false;
                    }

                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return Page();
                }
            }
            

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
