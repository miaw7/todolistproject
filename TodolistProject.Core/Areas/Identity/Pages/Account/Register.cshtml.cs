﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using TodolistProject.Core.Models.UserModels;

namespace TodolistProject.Core.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<CommonUser> _signInManager;
        private readonly UserManager<CommonUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        public RegisterModel(RoleManager<IdentityRole> roleManager,
        UserManager<CommonUser> userManager,
            SignInManager<CommonUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email:")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "User name:")]
            public string UserName { get; set; }

            [Required]
            [StringLength(30, ErrorMessage = "The password must be {2}  {1}", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password:")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm:")]
            [Compare("Password", ErrorMessage = "The passwords do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Birth date:")]
            [DataType(DataType.Date)]
            public DateTime BirthDate { get; set; }

            [Display(Name = "Real name:")]
            public string RealName { get; set; }

            public bool UseCaptcha { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                if(Input.BirthDate < new DateTime(2000, 1, 1))
                {
                    ModelState.AddModelError(string.Empty, "You mast be older than 18.");
                }
                var user = new CommonUser { UserName = Input.UserName, Email = Input.Email,
                    BirthDate = Input.BirthDate, RealName = Input.RealName };

                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    await _signInManager.SignInAsync(user, isPersistent: false);

                    //Init default Role after registration
                    var NewUser = await _userManager.FindByNameAsync(user.UserName);
                    await _userManager.AddToRoleAsync(NewUser,"user");

                    return LocalRedirect(returnUrl);
                }
                else
                {
                    Input.UseCaptcha = false;
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
