﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TodolistProject.Core.Models.UserModels;

namespace TodolistProject.Core.Data
{
    public class ApplicationDbContext : IdentityDbContext<CommonUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
