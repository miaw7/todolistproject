﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodolistProject.Core.Models.UserModels
{
    public class CommonUser : IdentityUser
    {
        public string RealName { get; set; }
        public bool IsBanned { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
