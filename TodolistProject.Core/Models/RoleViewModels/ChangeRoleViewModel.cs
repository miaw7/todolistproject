﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodolistProject.Core.Models.UserModels;

namespace TodolistProject.Core.Models.RoleViewModels
{
    public class ChangeRoleViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public List<IdentityRole> AllRoles { get; set; }
        public string[][] UserRoles { get; set; }
        public List<CommonUser> AllUsers { get; set; }


    }
}
