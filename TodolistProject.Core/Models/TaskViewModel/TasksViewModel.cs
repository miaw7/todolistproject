﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodolistProject.Core.Models.TaskViewModel
{
    public class TasksViewModel
    {
        public List<TaskViewModel> Tasks { get; set; }
    }
}
