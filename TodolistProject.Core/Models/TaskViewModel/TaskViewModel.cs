﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TodolistProject.Domain.Enums;
namespace TodolistProject.Core.Models.TaskViewModel
{
    public class TaskViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name = "Task name:")]
        public string TaskName { get; set; }
        [Required]
        [Display(Name = "Task description:")]
        public string TaskDescription { get; set; }
        [Required]
        [Display(Name = "Task priority:")]
        public string Priority { get; set; }

        public string Status { get; set; }

        public List<string> SelectListPriority = new List<string>() { $"{UserTaskPriority.Strong}", $"{UserTaskPriority.Middle}", $"{UserTaskPriority.Weak}" };
    }
}
