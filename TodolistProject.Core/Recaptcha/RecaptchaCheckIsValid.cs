﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace TodolistProject.Core.Recaptcha
{
    public class RecaptchaCheckIsValid 
    {
        private readonly HttpClient _httpClient;

        public RecaptchaCheckIsValid()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://www.google.com");
        }

        public async Task<bool> Validate(string gRecaptchaResponse)
        {
            var content = new FormUrlEncodedContent(new[]
            {
            new KeyValuePair<string, string>("secret", "6Lf_hqkUAAAAAORe375SrNt9lOXtOwaepWDuwVFH"),
            new KeyValuePair<string, string>("response", gRecaptchaResponse)
        });

            var response = await _httpClient.PostAsync("/recaptcha/api/siteverify", content);
            var resultContent = await response.Content.ReadAsStringAsync();
            var captchaResponse = JsonConvert.DeserializeObject<RecaptchaResponse>(resultContent);

            return captchaResponse.Success;
        }
    }
}
