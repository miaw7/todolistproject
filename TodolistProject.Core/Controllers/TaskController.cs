﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TodolistProject.Core.Models.TaskViewModel;
using TodolistProject.Core.Models.UserModels;
using TodolistProject.Service;
using TodolistProject.Service.Dtos;

namespace TodolistProject.Core.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly UserTaskService _service;
        private readonly UserManager<CommonUser> _userManager;

        public TaskController(UserManager<CommonUser> userManager)
        {
            _userManager = userManager;
            _service = new UserTaskService();
        }

        public IActionResult CreateTask()
        {
            TaskViewModel model = new TaskViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTask([FromBody]TaskViewModel task)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user.IsBanned == true)
            {
                Response.Redirect("../Home/Bunned");
            }
            var userTaskDto = new UserTaskDto();
            userTaskDto.UserId = user.Id;
            userTaskDto.TaskDescription = task.TaskDescription;
            userTaskDto.TaskName = task.TaskName;
            userTaskDto.Priority = task.Priority;
            _service.AddTask(userTaskDto);
            return Ok(task);
        }

        public async Task<IActionResult> ShowAllTasks(string filter)
        {
            TasksViewModel AllTask = new TasksViewModel();
            var user = await _userManager.GetUserAsync(HttpContext.User);
            List<UserTaskDto> serviceAllTasks = _service.GetAllTasks(user.Id);
            List<TaskViewModel> models = new List<TaskViewModel>();
            foreach (var task in serviceAllTasks)
            {
                if (filter == null || task.Priority == filter || filter == "All")
                {
                    TaskViewModel model = new TaskViewModel();
                    model.Id = task.Id;
                    model.TaskName = task.TaskName;
                    model.TaskDescription = task.TaskDescription;
                    model.Priority = task.Priority;
                    model.Status = task.Status;
                    models.Add(model);
                }
            }
            AllTask.Tasks = models;
            return View(AllTask);
        }
        [HttpPost]
        public IActionResult DeleteTask(string id)
        {
            _service.RemoveTask(id);
            return Json(id);
        }

        public IActionResult FinishTask()
        {

            return RedirectToAction("Index", "Home");
        }

    }
}