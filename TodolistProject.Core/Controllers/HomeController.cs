﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TodolistProject.Core.Models;
using TodolistProject.Core.Models.TaskViewModel;
using TodolistProject.Core.Models.UserModels;
using TodolistProject.Domain.Models;
using TodolistProject.Service;
using TodolistProject.Service.Dtos;

namespace TodolistProject.Core.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserTaskService _service;
        private readonly UserManager<CommonUser> _userManager;

        public HomeController(UserManager<CommonUser> userManager)
        {
            _service = new UserTaskService();
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Banned()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
