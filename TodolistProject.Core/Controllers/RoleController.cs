﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TodolistProject.Core.Models.RoleViewModels;
using TodolistProject.Core.Models.UserModels;
using TodolistProject.Service;

namespace TodolistProject.Core.Controllers
{
    [Authorize(Roles = "admin")]
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<CommonUser> _userManager;

        public RoleController(RoleManager<IdentityRole> roleManager, UserManager<CommonUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public async Task<IActionResult> ShowAllRoles()
        {
            ChangeRoleViewModel model = new ChangeRoleViewModel();
            model.AllUsers = _userManager.Users.ToList();
            model.UserRoles = new string[model.AllUsers.Count][];
            for (int i = 0; i < model.AllUsers.Count; i++)
            {
                var UserRole = await _userManager.GetRolesAsync(model.AllUsers[i]);
                model.UserRoles[i] = new string[UserRole.Count];
                for (int j = 0; j < UserRole.Count; j++)
                {
                    model.UserRoles[i][j] = UserRole[j];
                }
            }
         
            var roles = _roleManager.Roles.ToList();
            model.AllRoles = roles;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var NewUser = await _userManager.FindByIdAsync(id);
            await _userManager.DeleteAsync(NewUser);
            UserService service = new UserService();
            service.DeleteUserData(id);
            
            return Json(id);
        }

        [HttpPost]
        public IActionResult BanUser(string id)
        {
            UserService service = new UserService();
            service.BanUser(id);
            return Json(id);
        }

        [HttpPost]
        public IActionResult UnbanUser(string id)
        {
            UserService service = new UserService();
            service.UnbanUser(id);
            return Json(id);
        }

        //чтобы добавить пользователя - убрать права сверху на роль и запустить
        public async Task<IActionResult> InitializeRoles()
        {
            string adminEmail = "rood@mail.ru";

            if (await _roleManager.FindByNameAsync("admin") == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (await _roleManager.FindByNameAsync("user") == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("user"));
            }
            if (await _userManager.FindByEmailAsync(adminEmail) != null)
            {
                CommonUser admin = await _userManager.FindByNameAsync(adminEmail);
                var userRoles = await _userManager.GetRolesAsync(admin);
                await _userManager.AddToRoleAsync(admin, "admin");
                return Content("succeed");
            }
            else
                return Content("failure");

        }
    }
}