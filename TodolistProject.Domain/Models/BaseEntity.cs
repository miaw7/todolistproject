﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodolistProject.Domain.Models
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.Now;
        }
        [ExplicitKey]
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
