﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodolistProject.Domain.Models
{
    public class RecaptchaLock : BaseEntity
    {
        public string UserIpAdress { get; set; }
        public int FailedAmount { get; set; }
        public DateTime? LockTime { get; set; }
    }
}
