﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodolistProject.Domain.Models
{
    public class UserTask : BaseEntity
    {
        public UserTask()
        {
            Status = "Normal";
        }
        public string TaskName { get; set; }
        public string TaskDescription { get; set; }
        public DateTime? FinishDate { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }
        public string Priority { get; set; }
    }
}
