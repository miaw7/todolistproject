﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodolistProject.Domain.Enums
{
    public enum UserTaskPriority
    {
        Strong = 0,
        Middle = 1,
        Weak = 2
    }
}
