﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodolistProject.Domain.Enums
{
    public enum UserTaskStatus
    {
        Normal = 0,
        Deleted = 1
    }
}
