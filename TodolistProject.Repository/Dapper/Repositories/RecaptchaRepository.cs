﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Domain.Models;
using TodolistProject.Repository.Dapper.Interfaces;

namespace TodolistProject.Repository.Dapper.Repositories
{
    public class RecaptchaRepository : BaseRepository<RecaptchaLock>, IRecaptchaRepository
    {
        public RecaptchaRepository() : base("RecaptchaLocks")
        {
        }

        public RecaptchaLock GetLockedUser(string UserIp)
        {
            string sql = $@"SELECT * FROM RecaptchaLocks WHERE UserIpAdress = @Ip ";
            var result = Connection.QueryFirstOrDefault<RecaptchaLock>(sql, new { Ip = UserIp });
            return result;
        }
    }
}
