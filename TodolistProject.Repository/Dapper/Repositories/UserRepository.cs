﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Domain.Models;
using TodolistProject.Repository.Dapper.Interfaces;

namespace TodolistProject.Repository.Dapper.Repositories
{
    public class UserRepository : BaseRepository<UserTask>, IUserRepository
    {
        public UserRepository():base("UserTasks")
        {
        }

        public IEnumerable<UserTask> GetAllTaskForLogginedUser(string UserId)
        {
            string sql = "SELECT * FROM UserTasks WHERE Status <> @Status AND UserId = @UserId";
            return Connection.Query<UserTask>(sql, new { Status = "Deleted", UserId = UserId });
        }

        public void BanUser(string Id)
        {
            string sql = "UPDATE AspNetUsers SET IsBanned = 1 WHERE Id = @UserId";
            Connection.Execute(sql, new { UserId = Id });
        }

        public void UnbanUser(string Id)
        {
            string sql = "UPDATE AspNetUsers SET IsBanned = 0 WHERE Id = @UserId";
            Connection.Execute(sql, new { UserId = Id });
        }

        public void DeleteUserData(string UserId)
        {
            string sql = "DELETE FROM UserTasks WHERE UserId = @Id";
            Connection.Execute(sql, new { Id = UserId });
        }
    }
}
