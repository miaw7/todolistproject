﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Config;
using TodolistProject.Domain.Models;

namespace TodolistProject.Repository.Dapper.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected IDbConnection Connection { get { return new SqlConnection(AppConfig.Constring); } }
        protected readonly string _tableName;

        public BaseRepository(string tableName)
        {
            _tableName = tableName;
        }

        public virtual void Add(T entity)
        {
            Connection.Insert<T>(entity);
        }

        public virtual void Remove(T entity)
        {
            string sql = "DELETE FROM " + _tableName + " WHERE Id=@Id";
            Connection.Execute(sql, new { Id = entity.Id });
        }

        public virtual void Update(T entity)
        {
            Connection.Update(entity);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Connection.GetAll<T>();
        }

        public virtual T GetById(string id)
        {

            return Connection.Get<T>(id);
        }
    }
}
