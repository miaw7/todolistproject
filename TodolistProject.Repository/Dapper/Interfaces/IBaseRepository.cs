﻿using System.Collections.Generic;
using TodolistProject.Domain.Models;

namespace TodolistProject.Repository.Dapper.Repositories
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        void Add(T entity);
        void Remove(T entity);
        void Update(T entity);
        IEnumerable<T> GetAll();
    }
}