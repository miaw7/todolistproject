﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Domain.Models;

namespace TodolistProject.Repository.Dapper.Interfaces
{
    public interface IRecaptchaRepository
    {
        RecaptchaLock GetLockedUser(string UserIp);
    }
}
