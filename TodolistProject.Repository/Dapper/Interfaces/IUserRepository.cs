﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Domain.Models;

namespace TodolistProject.Repository.Dapper.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<UserTask> GetAllTaskForLogginedUser(string UserId);

        void BanUser(string Id);

        void UnbanUser(string Id);

        void DeleteUserData(string UserId);
    }
}
