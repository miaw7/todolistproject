﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodolistProject.Domain.Models;

namespace TodolistProject.Repository.EF
{
    public class TodolistContext : DbContext
    {
        public TodolistContext()
           : base("name=ToDoListContext")
        {

        }
        public virtual DbSet<UserTask> UserTasks { get; set; }
        public virtual DbSet<RecaptchaLock> RecaptchaLocks { get; set; }
    }
}
