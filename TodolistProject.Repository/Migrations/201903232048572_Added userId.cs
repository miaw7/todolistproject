namespace TodolistProject.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddeduserId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserTasks", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTasks", "UserId");
        }
    }
}
