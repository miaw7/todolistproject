namespace TodolistProject.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedrecaptchamodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RecaptchaLocks",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserIpAdress = c.String(),
                        FailedAmount = c.Int(nullable: false),
                        LockTime = c.DateTime(),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RecaptchaLocks");
        }
    }
}
