namespace TodolistProject.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedusertaskpriority : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserTasks", "Priority", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTasks", "Priority");
        }
    }
}
